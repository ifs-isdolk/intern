import React from "react";
import {DocumentCard, DocumentCardTitle} from '@fluentui/react/lib/DocumentCard';
import { DefaultButton, PrimaryButton, IButtonStyles} from '@fluentui/react/lib/Button'
import { mergeStyles } from '@fluentui/react/lib/Styling';
import { IIconProps, IContextualMenuProps, Stack, SelectableOptionMenuItemType, } from '@fluentui/react';
import { useBoolean } from '@fluentui/react-hooks';
import { ChoiceGroup, IChoiceGroupOption } from '@fluentui/react/lib/ChoiceGroup';
import {ComboBox, IComboBoxOption, IComboBoxStyles} from '@fluentui/react/lib/ComboBox';
import {Checkbox} from '@fluentui/react/lib/Checkbox'

const options: IChoiceGroupOption[] = [
  { key: 'day', text: 'Day', iconProps: { iconName: 'CalendarDay' } },
  { key: 'week', text: 'Week', iconProps: { iconName: 'CalendarWeek' } },
  { key: 'month', text: 'Month', iconProps: { iconName: 'Calendar' }, disabled: true },
];

const options_2: IComboBoxOption[] = [
  { key: 'Header1', text: 'First heading', itemType: SelectableOptionMenuItemType.Header },
  { key: 'A', text: 'Option A' },
  { key: 'B', text: 'Option B' },
  { key: 'divider', text: '-', itemType: SelectableOptionMenuItemType.Divider },
  { key: 'Header2', text: 'Second heading', itemType: SelectableOptionMenuItemType.Header },
  { key: 'E', text: 'Option E' },
  { key: 'F', text: 'Option F', disabled: true },
];

const container = {
    display: 'flex',
    justifyContent: 'center',
    margin: '5vh 0'
}

const DefaultButtonStyles = mergeStyles({
    margin: '0 10px',
});

const stackTokens = { childrenGap: 10 };

export interface IButtonExampleProps {
    disabled?: boolean;
    checked?: boolean;
  }

const volume0Icon: IIconProps = { iconName: 'Volume0' };
const volume3Icon: IIconProps = { iconName: 'Volume3' };

const DocumentCardStyles = mergeStyles ({
    padding: '20px',
    margin: 'auto',
    background: 'white',
    width: '100%',
    maxWidth: '100%'
});

const menuProps: IContextualMenuProps = {
    items: [
      {
        key: 'emailMessage',
        text: 'Email message',
        iconProps: { iconName: 'Mail' },
      },
      {
        key: 'calendarEvent',
        text: 'Calendar event',
        iconProps: { iconName: 'Calendar' },
      },
    ],
  };

  function _onChange(ev?: React.FormEvent<HTMLElement | HTMLInputElement>, isChecked?: boolean) {
    console.log(`The option has been changed to ${isChecked}.`);
  }

const comboBoxStyles: Partial<IComboBoxStyles> = { root: { maxWidth: 300 } };

const BasicInputs = () => {
    const [muted, { toggle: setMuted }] = useBoolean(false);

    return (
        <div style={container}>
            <DocumentCard className={DocumentCardStyles}>
                <DocumentCardTitle title = "Buttons"/>
                  <DefaultButton text="Standard" className={DefaultButtonStyles}/>
                  <PrimaryButton 
                    text="Primary Split Button |" 
                    className={DefaultButtonStyles}
                    primary split splitButtonAriaLabel="See 2 options"
                    aria-roledescription="split button"
                    menuProps={menuProps}
                  />
                  <DefaultButton className={DefaultButtonStyles}
                    toggle
                    checked={muted}
                    text={muted ? 'Volume muted' : 'Volume unmuted'}
                    iconProps={muted ? volume0Icon : volume3Icon}
                    onClick={setMuted}
                    allowDisabledFocus
                  />
                  <br/><br/>
                <DocumentCardTitle title = "Checkboxes"/>
                <Stack tokens={stackTokens}>
                  <Checkbox label="Indeterminate checkbox" defaultIndeterminate />
                  <Checkbox label="Unchecked checkbox" onChange={_onChange} />
                  <ChoiceGroup label="Pick one icon" defaultSelectedKey="day" options={options} />
                  <ComboBox
                    defaultSelectedKey="C"
                    label="Basic multi-select ComboBox"
                    multiSelect
                    options={options_2}
                    styles={comboBoxStyles}
                  />
                </Stack>

            </DocumentCard>
        </div>
    );
}

export default BasicInputs;


