import React from "react";
import {DocumentCard, DocumentCardTitle,} from '@fluentui/react/lib/DocumentCard';
import { FontIcon } from '@fluentui/react/lib/Icon';
import { mergeStyles } from '@fluentui/react/lib/Styling';
import {Text, initializeIcons} from '@fluentui/react';
import { Depths } from '@fluentui/theme';

const container = {
    display: 'flex',
    justifyContent: 'center',
    margin: '80vh 100px 0 20px'
}
const iconClass = mergeStyles({
    fontSize: 25,
    height: 25,
    width: 25,
    margin: '5px 25px',
    color: '#106ebe',
  });

const textClass = mergeStyles({
    margin: '0 15px',
});

const DocumentCardClass = mergeStyles({
    borderTop: '5px solid #106ebe',
    padding: '10px',
    margin: 'auto',
    background: 'white',
    width: '90%',
    maxWidth: '90%',
    boxShadow: Depths.depth16 
});

const cards = [
    {
        title: 'Current Balance',
        amount: '$87.75',
        icon: 'Money',
        percentage: '2.3',
    },
    {
        title: 'Current Expenses',
        amount: '$7876',
        icon: 'PaymentCard',
        percentage: '0.3',
    },
    {
        title: 'Current Income',
        amount: '$59775',
        icon: 'Savings',
        percentage: '1.3',
    },
]

const CardSection = () => {
    initializeIcons();
    return (
        <div style={container}>
          {cards.map((card) => (
              <div>
                  <DocumentCard className={DocumentCardClass}>
                    <div> <FontIcon aria-label="Compass" iconName={card.icon} className={iconClass} />
                    <DocumentCardTitle title={card.title} shouldTruncate />
                    <Text className={textClass}>Amount:{card.amount}</Text>
                    <Text className={textClass}>Percentage:{card.percentage}%</Text>
                    </div>
                </DocumentCard>
                <br></br>
              </div>
          ))}
        </div>
      );
}

export default CardSection;