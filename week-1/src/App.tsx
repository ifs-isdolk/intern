import './App.css';
import { Stack, IStackTokens, IStackItemStyles } from '@fluentui/react/lib/Stack';

import Navigation from './Navigation';
import CmdBar from './CmdBar';
import ContextualMenu from './ContextualMenu';
import CardSection from './CardSection';
import OperationsTable from './OperationsTable';
import BasicInputs from './BasicInputs';

const stackItemStyles: IStackItemStyles = {
  root: {
    alignItems: 'center',
    display: 'flex',
    height: '100vh',
    justifyContent: 'center',
  },
};

const stackTokens: IStackTokens = {
  childrenGap: 4,
};

function App() {
  return (
    <div>
        <Stack>
          <Stack.Item align="auto">
            <span><ContextualMenu/></span>
          </Stack.Item>
          <Stack.Item align="auto">
            <span><CmdBar/></span>
          </Stack.Item>
        </Stack>
        <Stack horizontal>
          <Stack.Item grow={0} styles={stackItemStyles}>
            <Navigation/>
          </Stack.Item>
          <Stack.Item grow={2} styles={stackItemStyles}>
            <Stack tokens={stackTokens}>
              <Stack.Item align="auto"><CardSection/></Stack.Item>
              <Stack.Item align="auto"><OperationsTable/></Stack.Item>
              <Stack.Item align="auto"><BasicInputs/></Stack.Item>
            </Stack>
          </Stack.Item>
        </Stack>
    </div>
  );
}

export default App;
