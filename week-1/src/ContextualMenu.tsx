import React from "react";
import { initializeIcons } from "@fluentui/react";
import { ContextualMenu, ContextualMenuItemType, IContextualMenuItem } from '@fluentui/react/lib/ContextualMenu';
import { mergeStyles, mergeStyleSets} from '@fluentui/react/lib/Styling';
import {DefaultButton} from '@fluentui/react/lib/Button';
import { css } from '@fluentui/react/lib/Utilities';

const refStyle = mergeStyles({
    textDecoration: 'none',
    color: 'primary'
})

const container = {
    display: 'flex',
    justifyContent: 'left',
    borderBottom: '2px solid #106ebe',
}

function renderCategoriesList(item: any): JSX.Element {
    return (
      <ul className={classNames.categoriesList}>
        <li className="ms-ContextualMenu-item">
          {item.categoryList.map((category: ICategoryList) => (
            <DefaultButton
              key={category.name}
              className={css('ms-ContextualMenu-link', classNames.button)}
              role="menuitemcheckbox"
            >
              <div>
                <span style={{ backgroundColor: category.color, width: 24, height: 24, verticalAlign: 'top' }} />
                <span className="ms-ContextualMenu-itemText">{category.name}</span>
              </div>
            </DefaultButton>
          ))}
        </li>
      </ul>
    );
  }
  
  interface ICategoryList {
    name: string;
    color: string;
  }

  const classNames = mergeStyleSets({
    menu: {
      textAlign: 'center',
      maxWidth: 180,
      selectors: {
        '.ms-ContextualMenu-item': {
          height: 'auto',
        },
      },
    },
    item: {
      display: 'inline-block',
      width: 40,
      height: 40,
      lineHeight: 40,
      textAlign: 'center',
      verticalAlign: 'middle',
      marginBottom: 8,
      cursor: 'pointer',
      selectors: {
        '&:hover': {
          backgroundColor: '#eaeaea',
        },
      },
    },
    categoriesList: {
      margin: 0,
      padding: 0,
      listStyleType: 'none',
    },
    button: {
      width: '40%',
      margin: '2%',
    },
  });

  
const menuProps: IContextualMenuItem[] = [
    {
        key: 'newItem',
        text: 'New',
        onClick: () => console.log('New clicked'),
    },
    {
        key: 'divider_1',
        itemType: ContextualMenuItemType.Divider,
    },
    {
        key: 'Later Today',
        iconProps: { iconName: 'Clock' },
        text: 'Later Today',
        secondaryText: '7:00 PM',
      },
      {
        key: 'Tomorrow',
        iconProps: { iconName: 'Coffeescript' },
        text: 'Tomorrow',
        secondaryText: 'Thu. 8:00 AM',
      },
      {
        key: 'divider_2',
        itemType: ContextualMenuItemType.Divider,
      },
      {
        key: 'openInWord',
        text: 'Open in Word',
        iconProps: { 
            iconName: 'WordLogo16',
         },
      },
      {
        key: 'divider_3',
        itemType: ContextualMenuItemType.Divider,
      },
      {
        key: 'categories',
        text: 'Categorize',
        ariaLabel: 'Categorize. Press enter, space or right arrow keys to open submenu.',
        subMenuProps: {
          items: [
            {
              key: 'categories',
              text: 'categories',
              categoryList: [
                { name: 'Personal', color: 'yellow' },
                { name: 'Work', color: 'green' },
                { name: 'Birthday', color: 'blue' },
                { name: 'Spam', color: 'grey' },
                { name: 'Urgent', color: 'red' },
                { name: 'Hobbies', color: 'black' },
              ],
              onRender: renderCategoriesList,
            },
            { key: 'divider_1', itemType: ContextualMenuItemType.Divider },
            { key: 'clear', text: 'Clear categories' },
            { key: 'manage', text: 'Manage categories' },
          ],
        },
      },
]

const Contextual_Menu = () => {    
    initializeIcons();
    const linkRef = React.useRef(null);
    const [showContextualMenu, setShowContextualMenu] = React.useState(false);
    const onShowContextualMenu = React.useCallback((ev: React.MouseEvent<HTMLElement>) => {
    ev.preventDefault(); 
    setShowContextualMenu(true);
  }, []);
  const onHideContextualMenu = React.useCallback(() => setShowContextualMenu(false), []);

    return (
        <div style={container}>
            <p>
        <b>
          <a ref={linkRef} onClick={onShowContextualMenu} href="#" className={refStyle}> 
            Click for Contextual Menu
          </a>
        </b>
      </p>
      <ContextualMenu
        items={menuProps}
        hidden={!showContextualMenu}
        target={linkRef}
        onItemClick={onHideContextualMenu}
        onDismiss={onHideContextualMenu}
      />
        </div>
    )
}

export default Contextual_Menu;