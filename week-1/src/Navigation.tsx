import { Nav, INavStyles, INavLinkGroup } from '@fluentui/react/lib/Nav';
import {initializeIcons} from '@fluentui/react';

const navStyles: Partial<INavStyles> = {
    root: {
        height: '100vh',
        boxSizing: 'border-box',
        border: '1px solid #eee',
        overflowY: 'auto',
    },
};

const navLinkGroups : INavLinkGroup[] = [
    {
        links: [
            {
                name: 'Dashboard',
                url: '/',
                key: 'key1',
                icon: "AssessmentGroup"
            },
            {
                name: 'Settings',
                url: '/',
                key: 'key2',
                icon: "PlayerSettings"
            },
            {
                name: 'Transfer',
                url: '/',
                key: 'key3',
                icon: "SwitcherStartEnd"
            },
            {
                name: 'Stats',
                url: '/',
                key: 'key4',
                icon: "AnalyticsView"
            },
            {
                name: 'News',
                url: '/',
                key: 'key5',
                icon: "News"
            },
        ],
    },
];


const Navigation = () => {
    initializeIcons();
    return(
        <Nav 
            groups={navLinkGroups}
            selectedKey = "key1"
            styles={navStyles}
        />
    )
}

export default Navigation;